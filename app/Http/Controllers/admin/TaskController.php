<?php

namespace App\Http\Controllers\admin;

use Auth;
use App\Model\Task;
use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Notifications\TaskAssigned;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::orderBy('created_at', 'desc')->paginate(50);
        return view('admin.task.index',compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $staffs = User::where('role','staff')->orderBy('name', 'asc')->get();
        return view('admin.task.create', compact('staffs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'staff_id' => 'required',
            'title' => 'required',
            'description' => 'required',
            'deadline' => 'required',
            'remarks' => 'sometimes',
        ]);
        $data = [
            'task_from' => Auth::user()->id,
            'task_to' => $request->staff_id,
            'title' => $request->title,
            'description' => $request->description,
            'accept_status' => 0,
            'status' => 'unaccept',
            'deadline' => $request->deadline,
            'remarks' => $request->remarks,
        ];
        $task = Task::create($data);
        $staff = User::find($request->staff_id);
        $staff->notify(new TaskAssigned($task));
        return redirect()->route('admin.task.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::find($id);
        foreach(auth()->user()->unreadNotifications as $notification){
            if($id == $notification->data['task_id']){
                Auth::user()->unreadNotifications->where('id', $notification->id)->markAsRead();
            }
        }
        return view('admin/task/view', compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::find($id);
        $staffs = User::where('role','staff')->orderBy('name', 'asc')->get();
        return view('admin.task.edit', compact('task', 'staffs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'staff_id' => 'required',
            'title' => 'required',
            'description' => 'required',
            'deadline' => 'required',
            'remarks' => 'sometimes',
        ]);
        $data = [
            'task_from' => Auth::user()->id,
            'task_to' => $request->staff_id,
            'title' => $request->title,
            'description' => $request->description,
            'accept_status' => 0,
            'status' => 'unaccept',
            'deadline' => $request->deadline,
            'remarks' => $request->remarks,
        ];
        Task::find($id)->update($data);
        return redirect()->route('admin.task.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Task::find($id)->delete();
        return redirect()->route('admin.task.index');
    }
}
