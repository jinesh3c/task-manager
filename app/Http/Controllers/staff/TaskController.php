<?php

namespace App\Http\Controllers\staff;

use Auth;
use App\Model\Task;
use App\Model\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Notifications\TaskCompleted;
use App\Notifications\TaskAccepted;

class TaskController extends Controller
{
    public function index()
    {
    	$tasks = Task::where('task_to', Auth::user()->id)->paginate(10);
    	return view('staff.task.index', compact('tasks'));
    }
    public function show($id)
    {
        $task = Task::find($id);
        foreach(auth()->user()->unreadNotifications as $notification){
            if($id == $notification->data['task_id']){
                Auth::user()->unreadNotifications->where('id', $notification->id)->markAsRead();
            }
        }
        return view('staff/task/view', compact('task'));
    }
    public function accept($id)
    {
    	Task::find($id)->update(['accept_status' => 1, 'status' => 'onprocess']);
        $task = Task::find($id);
        $staff = User::find($task->task_from);
        $staff->notify(new TaskAccepted($task));
    	return redirect()->back();
    }
    public function complete($id)
    {
    	$date = Carbon::now()->format('Y-m-d');
    	Task::find($id)->update(['completed_date' => $date, 'status' => 'complete']);
        $task = Task::find($id);
        $staff = User::find($task->task_from);
        $staff->notify(new TaskCompleted($task));
    	return redirect()->back();
    }
}
