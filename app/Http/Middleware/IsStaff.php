<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsStaff
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::User();
        $user_role = $user->role;
        if($user_role != 'staff'){
            return redirect('/login' );
        }
        return $next($request);
    }
}
