<?php

namespace App\Model;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $connection = 'mysql2';

    protected $fillable = [
        'title', 'task_from', 'task_to', 'description', 'accept_status', 'status', 'deadline', 'completed_date', 'remarks'
    ];

    public function task_to()
    {
    	return $this->belongsTo('\App\Model\User', 'task_to');
    }

    public static function unAcceptTask()
    {
    	return Task::where('task_to', Auth::user()->id)->where('accept_status', 0)->count();
    }
}
