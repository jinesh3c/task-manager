<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql2')->create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('task_from')->unsigned()->index();
            $table->foreign('task_from')->references('id')->on('test.users')->onDelete('cascade');
            $table->integer('task_to');
            $table->string('title');
            $table->text('description');
            $table->boolean('accept_status')->default(0);
            $table->enum('status',['unaccept','onprocess','complete']);
            $table->date('deadline');
            $table->date('completed_date')->nullable();
            $table->string('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
