<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use \App\Model\User;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'name' => 'admin',
        	'email' => 'admin@gmail.com',
        	'password' => Hash::make('password'),
        	'role' => 'admin',
        ]);
    }
}
