<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use \App\Model\User;
use Faker\Factory as Faker;

class StaffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();
    	foreach (range(1,10) as $index) {
	        DB::table('users')->insert([
	            'name' => $faker->name,
	            'email' => $faker->email,
	            'password' => Hash::make('password'),
	            'role' => 'staff'
	        ]);
	}
        
    }
}
