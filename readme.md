# Install
- git clone
- `composer install`
- create copy or rename .env.example
- `php artisan key:generate` 
- `php artisan migrate --seed`

# Project Detail:
- To Do Task Assignment

## Feature and functions:
- Multidatabase
- Database Notification

## Demo:
* admin: `admin@gmail.com`
* password: `password`