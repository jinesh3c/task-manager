@extends('layouts.app')

@section('content')
<link rel="stylesheet"
  href="{{asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="box">
                        <div class="box-title">
                            <h3>Create Staffs</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <form action="{{route('admin.task.update', $task->id)}}" class="form-horizontal" method="post">
                                        {!! csrf_field() !!}
                                        {!! method_field('PUT') !!}
                                        <div class="form-group">
                                            <label class="col-md-2">Title</label>
                                            <div class="col-md-10">
                                                <input type="text" name="title" class="form-control" placeholder="title" value="{{$task->title}}">
                                                @if ($errors->has('title'))
                                                    <span class="text-danger">
                                                        {{ $errors->first('title') }}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2">Select Staff</label>
                                            <div class="col-md-10">
                                                <select name="staff_id" id="staff_id" class="form-control">
                                                    <option value="">Select Staff</option>
                                                    @foreach($staffs as $staff)
                                                    <option value="{{$staff->id}}" @if($staff->id == $task->task_to) selected @endif>{{$staff->name}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('staff_id'))
                                                    <span class="text-danger">
                                                        {{ $errors->first('staff_id') }}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2">Description</label>
                                            <div class="col-md-10">
                                                <textarea name="description" class="form-control" placeholder="description">{{$task->description}}</textarea>
                                                @if ($errors->has('description'))
                                                    <span class="text-danger">
                                                        {{ $errors->first('description') }}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2">Deadline</label>
                                            <div class="col-md-10">
                                                <input type="text" name="deadline" id="deadline" value="{{$task->deadline}}" class="form-control" >
                                                @if ($errors->has('deadline'))
                                                    <span class="text-danger">
                                                        {{ $errors->first('deadline') }}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2">Remarks</label>
                                            <div class="col-md-10">
                                                <input type="text" name="remarks" class="form-control" value="{{$task->remarks}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2"></label>
                                            <div class="col-md-10">
                                                <input type="submit" class="btn btn-sm btn-primary" value="Submit">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('adminlte/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script>
    $('#deadline').datepicker({
        autoclose: true,
        format: 'yyyy-m-d',
        startDate: '-0d'
      })
</script> 
@endsection
