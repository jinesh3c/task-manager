@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="box">
                        <div class="box-title">
                            <h3>Staffs</h3>
                            <a href="{{route('admin.task.create')}}" class="btn btn-sm btn-primary pull-right">CREATE</a>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>SN</th>
                                            <th>Title</th>
                                            <th>Assigned To</th>
                                            <th>Status</th>
                                            <th>Deadline</th>
                                            <th>Action</th>
                                        </tr>
                                        @foreach($tasks as $k=>$task)
                                        <tr>
                                            <td>{{$k+1}}</td>
                                            <td>{{$task->title}}</td>
                                            <td>{{\App\Model\User::getName($task->task_to)}}</td>
                                            <td>
                                                @if($task->status=='unaccept')
                                                <label class="label bg-yellow">{{$task->status}}</label>
                                                @elseif($task->status=='onprocess')
                                                <label class="label bg-blue">{{$task->status}}</label>
                                                @elseif($task->status=='complete')
                                                <label class="label bg-green">{{$task->status}}</label>
                                                @else
                                                <label class="label bg-red">{{$task->status}}</label>
                                                @endif
                                            </td>
                                            <td>{{\Carbon\Carbon::parse($task->deadline)->format('d M, Y')}}</td>
                                            <td>
                                                <form action="{{route('admin.task.destroy', $task->id)}}" method="post">
                                                {!! csrf_field() !!}
                                                {!! method_field('DELETE') !!}  
                                                <a href="{{route('admin.task.show', $task->id)}}" class="btn btn-sm btn-success">VIEW</a> 
                                                @if($task->status != 'complete')
                                                <a href="{{route('admin.task.edit', $task->id)}}" class="btn btn-sm btn-info">EDIT</a> 
                                                @endif
                                                <button type="submit" class="btn btn-sm btn-danger">DELEET</button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </table>
                                    <div class="text-center">{{$tasks->links()}}</div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
