@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="box">
                        <div class="box-title">
                            <h3>Task View</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered">
                                    	<tr>
                                    		<td>Title</td>
                                    		<td>{{$task->title}}</td>
                                    	</tr>
                                    	<tr>
                                    		<td>Task Assigned By</td>
                                    		<td>{{\App\Model\User::getName($task->task_from)}}</td>
                                    	</tr>
                                    	<tr>
                                    		<td>Task Assigned To</td>
                                    		<td>{{\App\Model\User::getName($task->task_to)}}</td>
                                    	</tr>
                                    	<tr>
                                    		<td>Description</td>
                                    		<td>{{$task->description}}</td>
                                    	</tr>
                                    	<tr>
                                    		<td>Status</td>
                                    		<td>{{$task->status}}</td>
                                    	</tr>
                                    	<tr>
                                    		<td>Deadline</td>
                                    		<td>{{\Carbon\Carbon::parse($task->deadline)->format('d M, Y')}}</td>
                                    	</tr>
                                    	<tr>
                                    		<td>Completion Date</td>
                                    		<td>
                                    			@if($task->completed_date)
                                    			{{\Carbon\Carbon::parse($task->completed_date)->format('d M, Y')}}
                                    			@endif
                                    		</td>
                                    	</tr>
                                        <tr>
                                            <td>Remarks</td>
                                            <td>{{$task->remarks}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
