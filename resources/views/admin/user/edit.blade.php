@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="box">
                        <div class="box-title">
                            <h3>Create Staffs</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <form action="{{route('admin.staff.update', $staff->id)}}" class="form-horizontal" method="post">
                                        {!! csrf_field() !!}
                                        {!! method_field('PUT') !!}
                                        <div class="form-group">
                                            <label class="col-md-2">Name</label>
                                            <div class="col-md-10">
                                                <input type="text" name="name" class="form-control" placeholder="name" value="{{$staff->name}}">
                                                @if ($errors->has('name'))
                                                    <span class="text-danger">
                                                        {{ $errors->first('name') }}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2">Email</label>
                                            <div class="col-md-10">
                                                <input type="email" name="email" class="form-control" placeholder="xyz@mail.com" value="{{$staff->email}}">
                                                @if ($errors->has('email'))
                                                    <span class="text-danger">
                                                        {{ $errors->first('email') }}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2">Password</label>
                                            <div class="col-md-10">
                                                <input type="password" name="password" class="form-control" placeholder="password" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2">Role</label>
                                            <div class="col-md-10">
                                                <select name="role" id="role" class="form-control">
                                                    <option value="">Select Role</option>
                                                    <option value="admin" @if($staff->role=='admin') selected @endif>Admin</option>
                                                    <option value="staff" @if($staff->role=='staff') selected @endif>Staff</option>
                                                </select>
                                                @if ($errors->has('role'))
                                                    <span class="text-danger">
                                                        {{ $errors->first('role') }}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2"></label>
                                            <div class="col-md-10">
                                                <input type="submit" class="btn btn-sm btn-primary" value="Submit">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
