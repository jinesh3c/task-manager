@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="box">
                        <div class="box-title">
                            <h3>Staffs</h3>
                            <a href="{{route('admin.staff.create')}}" class="btn btn-sm btn-primary pull-right">CREATE</a>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>SN</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Role</th>
                                            <th>Action</th>
                                        </tr>
                                        @foreach($staffs as $k=>$staff)
                                        <tr>
                                            <td>{{$k+1}}</td>
                                            <td>{{$staff->name}}</td>
                                            <td>{{$staff->email}}</td>
                                            <td>{{$staff->role}}</td>
                                            <td>
                                                <form action="{{route('admin.staff.destroy', $staff->id)}}" method="post">
                                                {!! csrf_field() !!}
                                                {!! method_field('DELETE') !!}  
                                                <a href="{{route('admin.staff.edit', $staff->id)}}" class="btn btn-sm btn-info">EDIT</a> 
                                                <button type="submit" class="btn btn-sm btn-danger">DELEET</button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </table>
                                    <div class="text-center">{{$staffs->links()}}</div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
