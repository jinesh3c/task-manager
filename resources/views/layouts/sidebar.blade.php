<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>{{Auth::user()->name}}</p>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">s
      <li class="header">MAIN NAVIGATION</li>
      @if(Auth::user()->role == 'admin')
      <li @if(Request::segment(2)=='staff' ) class="active" @endif>
        <a href="{{route('admin.staff.index')}}">
          <i class="fa fa-users"></i> <span>Staff</span>
        </a>
      </li>
      <li @if(Request::segment(2)=='task' ) class="active" @endif>
        <a href="{{route('admin.task.index')}}">
          <i class="fa fa-th"></i> <span>Task</span>
        </a>
      </li>
      @else
      <li @if(Request::segment(2)=='task' ) class="active" @endif>
        <a href="{{route('staff.task.index')}}">
          <i class="fa fa-th"></i> <span>Task</span> <label class="label label-danger pull-right">{{\App\Model\Task::unAcceptTask()}}</label>
        </a>
      </li>
      @endif
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>