@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="box">
                        <div class="box-title">
                            <h3>Staffs</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>SN</th>
                                            <th>Title</th>
                                            <th>Assigned By</th>
                                            <th>Status</th>
                                            <th>Deadline</th>
                                            <th>Action</th>
                                        </tr>
                                        @foreach($tasks as $k=>$task)
                                        <tr>
                                            <td>{{$k+1}}</td>
                                            <td>{{$task->title}}</td>
                                            <td>{{\App\Model\User::getName($task->task_from)}}</td>
                                            <td>
                                                @if($task->status=='unaccept')
                                                <label class="label bg-yellow">{{$task->status}}</label>
                                                @elseif($task->status=='onprocess')
                                                <label class="label bg-blue">{{$task->status}}</label>
                                                @elseif($task->status=='complete')
                                                <label class="label bg-green">{{$task->status}}</label>
                                                @else
                                                <label class="label bg-red">{{$task->status}}</label>
                                                @endif
                                            </td>
                                            <td>{{\Carbon\Carbon::parse($task->deadline)->format('d M, Y')}}</td>
                                            <td>
                                                <a href="{{route('staff.task.show', $task->id)}}" class="btn btn-sm btn-primary">View</a>
                                                @if($task->accept_status == 0)
                                                <a href="{{route('staff.task.accept', $task->id)}}" class="btn btn-sm btn-info">Accept</a>
                                                @endif
                                                @if($task->status == 'onprocess')
                                                <a href="{{route('staff.task.complete', $task->id)}}" class="btn btn-sm btn-info">Complete</a>
                                                @else
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </table>
                                    <div class="text-center">{{$tasks->links()}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
