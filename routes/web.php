<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' =>['auth']], function(){
	Route::get('/home', 'HomeController@index')->name('home');
	
	Route::prefix('admin')->middleware('isAdmin')->name('admin.')->group(function() {
		Route::resource('/task', 'admin\TaskController');
		Route::resource('/staff', 'admin\UserController');
	});

	Route::prefix('staff')->middleware('isStaff')->group(function() {
		Route::get('/task', 'staff\TaskController@index')->name('staff.task.index');
		Route::get('/task/{id}', 'staff\TaskController@show')->name('staff.task.show');
		Route::get('/task/{id}/accept', 'staff\TaskController@accept')->name('staff.task.accept');
		Route::get('/task/{id}/complete', 'staff\TaskController@complete')->name('staff.task.complete');
	});
});

